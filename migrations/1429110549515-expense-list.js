var couch  = require('../lib/couch')
  , docc   = require('./designdoc/expense-000001')

exports.up = function(next){
    console.log('add expense show list')
    couch.getDb(function(couchdb){
        var userTables = couchdb.use('_users')
        userTables.view('user', 'by_db', function(err, body){
            body.rows.forEach(function(row){
                var table = row.key

                couch.addDoc({
                    dbName : table
                  , doccId : '_design/expense'
                  , docc   : docc
                }, function(){
                    console.log('------ complete insert _design/expense/view/list ------')
                    next()
                })

            })
        })
    })
};

exports.down = function(next){
    console.log('remove expense show list')
    couch.getDb(function(couchdb){
        var userTables = couchdb.use('_users')
        userTables.view('user', 'by_db', function(err, body){
            body.rows.forEach(function(row){
                var table = row.key

                couch.removeDoc({
                    dbName : table
                  , doccId : '_design/expense'
                }, function(){
                    console.log('complete: _design/expense reverted')
                    next()
                })

            })
        })
    })
};

