var couch  = require('../lib/couch')
  , dbName = '_users'
  , docc   = require('./designdoc/user-000001')

/*
 * To view
 *
 * // all user tables
 * couch.use('_users').view('user', 'by_db', function(err, view){
 *      if (err) return console.log(err)
 *      console.log(view.rows)
 * })
 *
 * // specific user table
 * couch.use('_users').view('user', {id: 'userId'}, 'by_db', function(err, view){
 *      if (err) return console.log(err)
 *      console.log(view.rows)
 * })
 *
 */

exports.up = function(next){
    console.log('------ migrate: user-getdb ------')
    couch.addDoc({
        dbName : dbName
      , doccId : '_design/user'
      , docc   : docc
    }, function(){
        console.log('------ complete insert _design/user/view/by_db ------')
        next()
    })
};

exports.down = function(next){
    console.log('------ user-getdb down ------')

    couch.removeDoc({
        dbName : dbName
      , doccId : '_design/user'
    }, function(){
        console.log('complete: _design/user reverted')
        next()
    })

};


