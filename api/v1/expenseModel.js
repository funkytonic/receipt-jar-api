export default function expenseModel(){
    var date = +new Date()
    return {
        _id          : 'rec-' + date
      , title        : null
      , desc         : null
      , amo          : '0.00' // amount
      , act          : null // action
      , loc          : null // location
      , type         : null
      , aud          : null // audience
      , createdAt    : date
      , lastUpdated  : date
      , files        : []
      , depreciation : null
      , status       : 'open'
    }
}

