import _      from 'lodash'
import co     from 'co'
import nanoDb from 'nano'
import coNano from 'co-nano'
import log    from '../../lib/logger'
import config from '../../config'
import expenseModel from './expenseModel'

var Expense = {}

/**
 * @api {get} /expneses Lists all expenses
 * @apiGroup Expenses
 * @apiVersion 1.0.0
 *
 * @apiSuccess {Object} Expense
 */
Expense.list = function *() {
    log.info('Getting receipt list for usr')

    var nano = getUserDb(this)

    this.body = yield co(function*(){
        var list =  yield nano.view('expense', 'list')
        log.info(list)
        return list[0].rows.map(function(item){
            return item.value
        })
    }).catch(function(err){
        log.error(err)
        return err
    })
}

/**
 * @api {get} /expnese/:id Get expense by id
 * @apiGroup Expenses
 * @apiVersion 1.0.0
 *
 * @apiParam {id} expenses Uid
 *
 * @apiSuccess {Object} Expense
 */
Expense.get = function *() {
    var nano   = getUserDb(this)
      , params = this.params

    this.body = yield co(function*(){
        var item = yield nano.get(params.id, {"all_docs":true})
        log.info(item)
        return item[0]

    }).catch(function(err){
        log.error(err)
        return err
    })
}

/**
 * @api {post} /expnese Add new expense
 * @apiGroup Expenses
 * @apiVersion 1.0.0
 *
 * @apiHeaderExample {json} post object
 *   {
 *       "title" : "Bought a mac"
 *     , "amo"   : "98.45"
 *     , "act"   : "claimable"
 *     , "type"  : "fun"
 *     , "amo"   : "98.45"
 *   }
 *
 * @apiParam  title           null
 * @apiParam  amo             '0.00' | amount
 * @apiParam  act             null   | action
 * @apiParam  type            null
 * @apiParam  [loc]           null   | location
 * @apiParam  [desc]          null
 * @apiParam  [aud]           null   | audience
 * @apiParam  [files]         []
 * @apiParam  [depreciation]  null
 * @apiParam  [status]        'open'
 *
 * @apiSuccess {Object} Expense
 */
Expense.add = function *() {
    var nano = getUserDb(this)
      , req = this.request.body

    log.info('add expense')

    if (_.isEmpty(req)) {
        this.status = 500
        this.body   = "Please provide data"
    }

    var expenseObj = _.extend(new expenseModel(), req)

    this.body = yield co(function*(){
        var list =  yield nano.insert(expenseObj)
        log.info(list)
        return {
            status : list[1].statusCode
          , data   : expenseObj
        }
    }).catch(function(err){
        log.error(err)
        return {
            status : err.status
          , err    : err
        }
    })
}


/**
 * @api {put} /expnese/:id update
 * @apiGroup Expenses
 * @apiVersion 1.0.0
 *
 * @apiParam :id Uid
 *
 * @apiHeaderExample {json} post object
 *   {
 *       "title" : "Bought a mac"
 *     , "amo"   : "98.45"
 *     , "act"   : "claimable"
 *     , "type"  : "fun"
 *     , "amo"   : "98.45"
 *   }
 *
 * @apiParam  title           null
 * @apiParam  amo             '0.00' | amount
 * @apiParam  act             null   | action
 * @apiParam  type            null
 * @apiParam  [loc]           null   | location
 * @apiParam  [desc]          null
 * @apiParam  [aud]           null   | audience
 * @apiParam  [files]         []
 * @apiParam  [depreciation]  null
 * @apiParam  [status]        'open'
 *
 * @apiSuccess {Object} Expense
 */
Expense.update = function *() {
    var params = this.params
      , req    = this.request.body
      , nano   = getUserDb(this)

    log.info('updating item: ' + params.id)

    this.body = yield co(function *(){
        var item = yield nano.get(params.id)
        log.info(item)

        req._id  = params.id
        req._rev = item[0]._rev

        var write = yield nano.insert(req)
        log.info(write)

        return {
             status : write[1].statusCode
           , data   : write[0]
        }

    }).catch(function(err){
        log.error(err)
        return err
    })
}

/**
 * @api {delete} /expnese/:id delete expense
 * @apiGroup Expenses
 * @apiVersion 1.0.0
 *
 * @apiParam :id Uid
 * @apiSuccess {String} success
 */
Expense.remove = function *() {

    var nano   = getUserDb(this)
      , params = this.params

    log.info('removing item: ', params.id)

    var item = yield nano.get(params.id)
    log.info(item)

    this.body = yield co(function*(){
        yield nano.destroy(params.id, item[0]._rev)
        return params.id + ' deleted'
    }).catch(function(err){
        return err
    })
}

export default Expense

// Get user db from auth token
function getUserDb(data) {
    var db      = data.user.db
      , session = data.user.session

    return coNano(nanoDb({url: config.couch.url, cookie: session})).use(db)
}

