import _         from 'lodash'
import co        from 'co'
import nanoDb    from 'nano'
import coNano    from 'co-nano'
import base64    from 'Base64'
import bcrypt    from 'co-bcrypt'
import userModel from './userModel'
import config    from '../../config'
import jwt       from 'koa-jwt'
import log       from '../../lib/logger'

var url        = config.couch.url
  , dbNameSalt = config.dbNameSalt
  , nano       = coNano(nanoDb(url))
  , user

/**
 * _adminDb
 * return {Object} coNano(url)
 *
 * Usage:
 * yield _adminDb()
 */
function * _adminDb() {
    var auth = yield co(function *(){
        return yield nano.auth('admin','admin')
    })
    .catch(function(err){
        return err.message
    })
    return coNano(nanoDb({url:url, cookie: auth[1]['set-cookie']}))
}

/**
 * getUserDatabase
 * retrieve the hashed database id for user
 *
 * @param {String} username
 */
function * _getUserDatabase(username) {
    var encode = yield bcrypt.hash(username, dbNameSalt)
    return base64.btoa(encode).toLowerCase()
}

function * createNewUser(data) {
    log.error('Creating New User & database')
    return co(function *(){
        var user = yield _createUser(data)
        yield _createDbForUser(user.db)

        var nano = yield _adminDb()

        var database = nano.use(user.db)
        yield database.insert({
            members : {
                names  : []
                ,roles : [user.name]
            }
            ,admins : {
                names  : [user.name]
                ,roles : []
            }
        }, '_security')

        
        yield database.insert({
            validate_doc_update : "function(new_doc, old_doc, userCtx) { if (userCtx.name != '"+user.name+"' && userCtx.roles.indexOf('"+user.name+"') == -1) { throw({forbidden: 'Not Authorized'}); } }"
        }, '_design/security')

        return "user has been created"
    })
    .catch(function(err){
        log.error(err)
        return err
    })
}

function * _createUser(data) {
    log.info('Create User')
    return yield co(function *(){
        var nano = yield _adminDb()
        var _users = nano.use('_users')

        user = _.clone(userModel)
        Object.assign(user, {
            _id      : 'org.couchdb.user:'+data.username
            , name     : data.username
            , password : data.password
            , type     : 'user'
            , roles    : [data.username]
        })

        user.db = yield _getUserDatabase(user.name)

        // insert into couch
        yield _users.insert(user)

        // remove password field in user object to return
        delete user.password

        log.info(user)
        return user
    })
    .catch(function(err){
        log.error(err)
        if (err.error === 'conflict') {
            return 'user already exists'
        }
        else 
        {
            return err
        }
    })
}

function * _createDbForUser(dbname) {
    var nano = yield _adminDb()

    return yield co(function *(){
        yield nano.db.create(dbname)
    })
    .catch(function(err){
        log.error('Create database for user: =================')
        log.error(err)
        return err
    })
}

function * login(data) {
    log.info('/login :')
    log.info(data)
    return co(function *(){
        var authUser = yield nano.auth(data.username, data.password)
          , session  = authUser[1]['set-cookie'][0]
          , userNano = coNano(nanoDb({url:url, cookie: session}))


        var user = {
            session : session
        }

        var userDb = yield userNano.use('_users').get('org.couchdb.user:' + data.username )
            userDb = userDb[0]

        // return only the public fields from user model
        for (var key in userModel) {
            user[key] = userDb[key]
        }

        // create token
        var claim = user
          , token = _generateToken(claim)

        log.info('User login success')
        log.info({ user : user })
        return token
    }).catch(function(err){
        log.error(err)
        return ('401, Wrong username or password')
    })
}

/**
 * Check is token is authenticated
 * if not, block it and return unauthenticated
 */
function* isLoggedin(next) {
    log.info('Check user is logged in')
    var token = this.header.authorization
    this.user = false

    if (token) {
        this.user = yield co(function* (){
            var user = this.user = yield jwt.verify(token, config.secret)
            log.info({user:user})
            return user
        }).catch(function(err) {
            return false
        })
    }

    if (!this.user) {
        log.error('unauthenticated excess')
        this.status = 401
        this.body   = "unauthenticated"
        return
    }

    yield next
}

function* renewToken(oldJwt, newSession) {
    // update user
    var user = yield jwt.verify(oldJwt, config.secret)
    user.session = newSession

    // create token
    token = _generateToken(claim)

    log.info('Renew user token')
    return token
}

function* _generateToken(claimObj) {
    return jwt.sign(claimObj, config.secret, {
        expiresInMinutes : 60 * 2
    })
}

export {
      createNewUser
    , isLoggedin
    , login
    , renewToken
}

