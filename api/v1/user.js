import co  from 'co'
import _   from 'lodash'
import jwt from 'koa-jwt'
import {login, createNewUser} from './auth'

var User = {}

/**
 * @api {post} /register  Register user
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription Register user to database, 
 * and create database for user
 *
 * @apiHeaderExample {json} post object
 *   {
 *       "username" : "test@test.com"
 *     , "password" : "password"
 *   }
 *
 * @apiParam {String} username Should be email
 * @apiParam {String} password Password
 * @apiParam {String} [Firstname] First name
 * @apiParam {String} [lastname] Last Name
 *
 * @apiSuccess {String} cookie jwt, required to use as login header
 */
User.register = function *() {
    var req = this.request.body

    if (!req || (!req.username && !req.password) ) {
        this.body = "Invalid username & password" 
        return
    }

    this.body = yield createNewUser({
         username : req.username
       , password : req.password
    })
}

/**
 * @api {post} /login Login user
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription Retrieve user JWT from login
 * once retrieved, JWT expires in 30min, all loggined required path needs header
 * "Authorization" = token 
 *
 * @apiHeaderExample {json} token example
 *   {
 *      "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..............."
 *   }
 *
 * @apiSuccess {Object} Expense
 */
User.login = function *() {
    var req = this.request.body
    if (!req || (!req.username && !req.password) ) {
        this.body = "Invalid username & password" 
        return
    }

    var userObj = yield login({
         username : req.username
       , password : req.password
    })

    this.body = userObj
}


/**
 * @api {get} /profile User profile get
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription Get user profile data
 *
 * @apiSuccess {String} cookie
 */
User.getUser = function *() {
    this.body = "user login"   
}

/**
 * @api {post} /profile User profile update
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiDescription Register user to database, 
 * and create database for user
 *
 * @apiParam {String} [username] Should be email
 * @apiParam {String} [password] Password
 * @apiParam {String} [Firstname] First name
 * @apiParam {String} [lastname] Last Name
 * @apiParam {String} [plan] user plan grade
 *
 * @apiSuccess {json} updated user object
 */
User.updateUser = function *() {
    this.body = "user login"   
}

export default User

