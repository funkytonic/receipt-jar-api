import Router       from 'koa-router'
import Expense      from './expense'
import User         from './user'
import {isLoggedin} from './auth'

var app = new Router()


// # User
app.post ('/login'   ,             User.login)      //| login usr
app.post ('/register', isLoggedin, User.register)   //| register user, db for user
app.get  ('/profile' , isLoggedin, User.getUser)    //| Get profile
app.post ('/profile' , isLoggedin, User.updateUser) //| Update profile

// # Expenses
app.get    ('/expenses'    , isLoggedin , Expense.list)   //| list all expenses
app.get    ('/expense/:id' , isLoggedin , Expense.get)    //| get expense by id
app.post   ('/expense'     , isLoggedin , Expense.add)    //| add / update expenses
app.put    ('/expense/:id' , isLoggedin , Expense.update) //| get expense by id
app.delete ('/expense/:id' , isLoggedin , Expense.remove) //| delete expense

export default app.middleware()

