var config = require('../config')
  , nano   = require('nano')

var sessionKey

// get the admin permision to the couchdb
function getDb(cb) {
    nano(config.couch.url).auth(config.couch.user,config.couch.pass, function(err,body,header){
        var cookie = sessionKey = header['set-cookie']
        cb(nano({url:config.couch.url, cookie: cookie}))
    })
}

// add or update design doc
// option: [ docc, id, dbName ]
// done
function addDoc(option, done) {
    getDb(function(couchdb){
        var db=couchdb.use(option.dbName)
        db.get(option.doccId, function(err,body){
            // if it already exists
            if (!err) 
            {
                option.docc._rev = body._rev
            }

            db.insert(option.docc, option.doccId, function(err){
                if (err) return console.log(err);
                done()
            })
        })
    })
}

function removeDoc(option, done) {
    getDb(function(couchdb){
        var db=couchdb.use(option.dbName)
        db.get(option.doccId, function(err, body){
            db.destroy(option.doccId, body._rev, function(err, body){
                if (err) return console.log(err)
                done();
            })
        })
    })
}


module.exports = {
    getDb     : getDb
  , addDoc    : addDoc
  , removeDoc : removeDoc
}

