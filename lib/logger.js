import bunyan    from 'bunyan'

export default bunyan.createLogger({
    name : 'API'
    ,streams : [
        {path : './LOGS'}
    ]
})

