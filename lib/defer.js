// defer of a promise
//
// var def = new Defer()
//
// // wait for defer
// def.then(function(){})
//
// to resolve
// defer.resolve('value') 

export default class Defer {
    constructor(method, parent) {
        var self = this
        self.promise = new Promise(function(resolve, reject){
            self.resolve = resolve
            self.reject  = reject
        })
        return this
    }
}

