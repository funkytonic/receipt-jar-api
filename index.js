import koa      from 'koa'
import logger   from 'koa-logger'
import Router   from 'koa-router'
import cors     from 'koa-cors'
import mount    from 'koa-mount'
import session  from 'koa-session'
import koa_body from 'koa-body'
import co       from 'co'

import config from './config'


// Config
var app = koa()

app.keys = [config.secret]

app.use(cors())
app.use(logger())
app.use(koa_body())
app.use(session(app))
app.use(Router(app))

// index page
app.get('/', function *(){
    this.body = 'Sorry this is unavailable.'
})

import APIv1 from './api/v1'
app.use(mount('/v1', APIv1))

app.listen(3334)

